/*
 * TeleStax, Open Source Cloud Communications
 * Copyright 2011-2016, Telestax Inc and individual contributors
 * by the @authors tag.
 *
 * This program is free software: you can redistribute it and/or modify
 * under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @author pavel.shlupacek@spinoco.com
 */

package org.restcomm.media.resource.recorder.audio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.log4j.Logger;

/**
 * Sink, that assures the data are written to underlying file.
 *
 * Sink exists once the recording starts, and ceases to exists on recording deactivate.
 *
 * @author Pavel Chlupacek (pchlupacek)
 */
public class DefaultRecorderFileSink extends RecorderFileSink{

    private static final Logger logger = Logger.getLogger(DefaultRecorderFileSink.class);
    private byte[] encodedBytes = new byte[5242880];

    /**
     * Creates a sink. If append is true, and target exists, then when recording is finished the resulting recording is appended
     * to current recorded file.
     * 
     * @param target Target to write file to
     * @param append Whether to append recording to `target`
     */
    public DefaultRecorderFileSink(Path target, boolean append) throws IOException {
	super(target, append);
    }

    /**
     * Writes supplied data to the Sink (File).
     */
    public void write(ByteBuffer data) throws IOException {
        if (open.get()) {
        	byte[] dataArray = new byte[data.remaining()];
        	data.get(dataArray);

        	int encodedlength = encoder.getAudioEncoder().encodeBuffer(dataArray, 0,
        			data.limit(), encodedBytes);
        	byte[] tmp = new byte[encodedlength];
        	System.arraycopy(encodedBytes, 0, tmp, 0, encodedlength);
        	ByteBuffer buffer = ByteBuffer.wrap(tmp);
        	fout.write(buffer);
        }
    }
    
    /**
     * Commit this sink. Causes to prevent any further write operations, and commits temporary file to target. When this
     * returns, Sink is done and cannot be used again.
     */
    public void commit() throws IOException {
    	
        // assures we perform the close operation only once.
        if (open.compareAndSet(true, false)) {
            // if the current file exists, and append is true, then append samples and remove temp file
            // otherwise write header and move tmp file to target
            boolean exists = Files.exists(target);
            if (logger.isInfoEnabled()) {
                logger.info("Finishing recording ...... append: " + append + " exists: " + exists + " target:" + target);
            }
            if (append && exists) {
                appendSamples(target, temp);
                writeHeader(target);
                Files.delete(temp);
            } else {
            	encoder.getAudioEncoder().encodeFinish(encodedBytes);
        		encoder.getAudioEncoder().close();
                Files.move(temp, target, StandardCopyOption.REPLACE_EXISTING);
            }
            
         // flush & close
            fout.force(false);
            fout.close();
        }
    }



}
