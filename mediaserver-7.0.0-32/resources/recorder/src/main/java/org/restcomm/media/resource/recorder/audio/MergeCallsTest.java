package org.restcomm.media.resource.recorder.audio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MergeCallsTest {

   
    public static void main(String[]args) throws IOException {
        ByteBuffer byteBuffer;
        
    	Path path1 = Paths.get("E:/Issues/VOX-721/Test2/98c114ea-dcab-4974-909a-06312172e609_a.mp3");
    	Path path2 = Paths.get("E:/Issues/VOX-721/Test2/d81e5cfc-072a-4450-8f4e-daf15f110d15__a.mp3");
    	Path result= Paths.get("E:/Issues/VOX-721/Test2/Audio3.mp3") ;
    	
        byte[] byte1 = Files.readAllBytes(path1);
        byte[] byte2 = Files.readAllBytes(path2);
        byte[] out = new byte[byte1.length];

        for (int i=0; i<byte1.length; i++)
            out[i] = (byte) ((byte1[i] + byte2[i]) >> 1);

        byteBuffer = ByteBuffer.wrap(out);
        
	RecorderFileSink sink = new MixRecorderFileSink(result, false);
        sink.write(byteBuffer);
        sink.commit();
    }
}
