package org.restcomm.media.resource.recorder.audio;

/*
 * TeleStax, Open Source Cloud Communications
 * Copyright 2011-2016, Telestax Inc and individual contributors
 * by the @authors tag.
 *
 * This program is free software: you can redistribute it and/or modify
 * under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @author pavel.shlupacek@spinoco.com
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.log4j.Logger;

/**
 * Sink, that assures the data are written to underlying file.
 *
 * Sink exists once the recording starts, and ceases to exists on recording
 * deactivate.
 *
 * @author Pavel Chlupacek (pchlupacek)
 */
public class MixRecorderFileSink extends RecorderFileSink{

    private static final Logger logger = Logger.getLogger(MixRecorderFileSink.class);
    private byte[] encodedBytes = new byte[5242880];



    /**
     * Creates a sink. If append is true, and target exists, then when recording
     * is finished the resulting recording is appended to current recorded file.
     * 
     * @param target
     *            Target to write file to
     * @param append
     *            Whether to append recording to `target`
     */
    public MixRecorderFileSink(Path target, boolean append) throws IOException {
	super(target, append);
    }

    /**
     * Writes supplied data to the Sink (File).
     */
    public void write(ByteBuffer data) throws IOException {
	if (open.get()) {
	    fout.write(data);
	}
    }

    /**
     * Commit this sink. Causes to prevent any further write operations, and
     * commits temporary file to target. When this returns, Sink is done and
     * cannot be used again.
     */
    public void commit() throws IOException {
	synchronized (MixRecorderFileSink.class) {
	    // assures we perform the close operation only once.
	    if (open.compareAndSet(true, false)) {
		// if the current file exists, and append is true, then append
		// samples and remove temp file
		// otherwise write header and move tmp file to target
		boolean exists = Files.exists(target);
		if (logger.isInfoEnabled()) {
		    logger.info("Finishing recording ...... append: " + append + " exists: " + exists + " target:"
			    + target);
		}
		if (exists) {
		    mergeCalls(target, temp);
		    Files.delete(temp);
		    encoder.getAudioEncoder().encodeFinish(encodedBytes);
		    encoder.getAudioEncoder().close();
		} else {
		    Files.move(temp, target, StandardCopyOption.REPLACE_EXISTING);
		}

		// flush & close
		fout.force(false);
		fout.close();
	    }
	}
    }

    /**
     * 
     * @param destinationPath
     * @param sourcePath
     * @throws IOException
     */
    private void mergeCalls(Path destinationPath, Path sourcePath) throws IOException {

	byte[] byte1 = Files.readAllBytes(sourcePath);
	byte[] byte2 = Files.readAllBytes(destinationPath);
	Files.delete(destinationPath);
	byte[] out = new byte[byte1.length];
	for (int i = 0; i < byte2.length; i++) {
	    if (i < byte1.length)
		out[i] = (byte) ((byte1[i] + byte2[i]) >> 1);
	}
	int encodedlength = encoder.getAudioEncoder().encodeBuffer(out, 0, out.length, encodedBytes);
	byte[] tmp = new byte[encodedlength];
	System.arraycopy(encodedBytes, 0, tmp, 0, encodedlength);
	FileOutputStream fileOutputStream = new FileOutputStream(destinationPath.toString());
	fileOutputStream.write(tmp);
	fileOutputStream.close();
    }

}
