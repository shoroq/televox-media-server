package org.restcomm.media.resource.recorder.audio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;

import org.apache.log4j.Logger;

import com.telelogx.media.encoder.AudioCodec;
import com.telelogx.media.encoder.ChannelType;
import com.telelogx.media.encoder.Encoder;
import com.telelogx.media.encoder.MetaData;
import com.telelogx.media.encoder.RateType;
import com.telelogx.media.encoder.impl.LameAudioEncoder;

public abstract class RecorderFileSink {

    private static final Logger logger = Logger.getLogger(RecorderFileSink.class);
    private static final int HDR_SIZE = 44;
    private static final ByteBuffer EMPTY_HEADER = ByteBuffer.wrap(new byte[HDR_SIZE]).asReadOnlyBuffer();

    // target and temp file used for recording
    protected final Path target;
    protected final Path temp;

    // whether the recording shall be appended to target, if that target exists
    protected final boolean append;

    // destination of write operation
    protected final FileChannel fout;

    // when true, then this sink accepts new data false otherwise.
    protected final AtomicBoolean open;

    protected Encoder encoder;

    /**
     * Creates a sink. If append is true, and target exists, then when recording
     * is finished the resulting recording is appended to current recorded file.
     * 
     * @param target
     *            Target to write file to
     * @param append
     *            Whether to append recording to `target`
     */
    public RecorderFileSink(Path target, boolean append) throws IOException {
	this.target = target;
	this.temp = target.getParent().resolve(target.getFileName() + UUID.randomUUID().toString() + "~");
	this.append = append;
	this.open = new AtomicBoolean(true);

	this.fout = FileChannel.open(temp, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW);
	this.fout.write(EMPTY_HEADER);

	this.encoder = createAudioOnlyEncoder(getSourceFormat());
    }

    /**
     * @return
     */
    public AudioFormat getSourceFormat() {
	AudioFormat sourceFormat;
	Encoding encoding = AudioFormat.Encoding.PCM_SIGNED;
	float sampleRate = 8000.0F;
	int sampleSizeInBits = 16;
	int channels = 1;
	int frameSize = sampleSizeInBits / (8 * channels);
	float frameRate = 8000.0F;
	boolean bigEndian = false;
	sourceFormat = new AudioFormat(encoding, sampleRate, sampleSizeInBits, channels, frameSize, frameRate,
		bigEndian);
	return sourceFormat;
    }

    /**
     * @param sourceFormat
     * @return
     */
    public Encoder createAudioOnlyEncoder(AudioFormat sourceFormat) {
	LameAudioEncoder audioIncoder = new LameAudioEncoder(sourceFormat);
	MetaData metaData = new MetaData(RateType.lookup((int) sourceFormat.getSampleRate()),
		ChannelType.lookup(sourceFormat.getChannels()), AudioCodec.MP3, null);
	Encoder encoder = new Encoder(audioIncoder, null, metaData);
	return encoder;
    }

    /**
     * @param appendTo
     * @param appendFrom
     * @throws IOException
     */
    public static void appendSamples(Path appendTo, Path appendFrom) throws IOException {

	try (FileChannel inChannel = FileChannel.open(appendFrom, StandardOpenOption.READ);
		FileChannel outChannel = FileChannel.open(appendTo, StandardOpenOption.WRITE)) {
	    long count = inChannel.size() - HDR_SIZE;
	    inChannel.transferTo(HDR_SIZE, count, outChannel);
	    if (logger.isInfoEnabled()) {
		logger.info("Appended " + count + " bytes from " + appendFrom + " to " + appendTo);
	    }
	}
    }

    /**
     * Writes samples to file following WAVE format.
     *
     *
     * @param file
     *            Recording where to write the header
     *
     * @throws IOException
     */
    protected static void writeHeader(Path file) throws IOException {
	try (FileChannel fout = FileChannel.open(file, StandardOpenOption.WRITE)) {

	    long size = fout.size();
	    int sampleSize = (int) size - 44;

	    if (logger.isInfoEnabled()) {
		logger.info("Size  " + sampleSize + " of recording file " + file);
	    }

	    ByteBuffer headerBuffer = ByteBuffer.allocateDirect(44);
	    headerBuffer.clear();
	    // RIFF
	    headerBuffer.put((byte) 0x52);
	    headerBuffer.put((byte) 0x49);
	    headerBuffer.put((byte) 0x46);
	    headerBuffer.put((byte) 0x46);

	    int length = sampleSize + 36;

	    // Length
	    headerBuffer.put((byte) (length));
	    headerBuffer.put((byte) (length >> 8));
	    headerBuffer.put((byte) (length >> 16));
	    headerBuffer.put((byte) (length >> 24));

	    // WAVE
	    headerBuffer.put((byte) 0x57);
	    headerBuffer.put((byte) 0x41);
	    headerBuffer.put((byte) 0x56);
	    headerBuffer.put((byte) 0x45);

	    // fmt
	    headerBuffer.put((byte) 0x66);
	    headerBuffer.put((byte) 0x6d);
	    headerBuffer.put((byte) 0x74);
	    headerBuffer.put((byte) 0x20);

	    headerBuffer.put((byte) 0x10);
	    headerBuffer.put((byte) 0x00);
	    headerBuffer.put((byte) 0x00);
	    headerBuffer.put((byte) 0x00);

	    // format - PCM
	    headerBuffer.put((byte) 0x01);
	    headerBuffer.put((byte) 0x00);

	    // format - MONO
	    headerBuffer.put((byte) 0x01);
	    headerBuffer.put((byte) 0x00);

	    // sample rate:8000
	    headerBuffer.put((byte) 0x40);
	    headerBuffer.put((byte) 0x1F);
	    headerBuffer.put((byte) 0x00);
	    headerBuffer.put((byte) 0x00);

	    // byte rate
	    headerBuffer.put((byte) 0x80);
	    headerBuffer.put((byte) 0x3E);
	    headerBuffer.put((byte) 0x00);
	    headerBuffer.put((byte) 0x00);

	    // Block align
	    headerBuffer.put((byte) 0x02);
	    headerBuffer.put((byte) 0x00);

	    // Bits per sample: 16
	    headerBuffer.put((byte) 0x10);
	    headerBuffer.put((byte) 0x00);

	    // "data"
	    headerBuffer.put((byte) 0x64);
	    headerBuffer.put((byte) 0x61);
	    headerBuffer.put((byte) 0x74);
	    headerBuffer.put((byte) 0x61);

	    // len
	    headerBuffer.put((byte) (sampleSize));
	    headerBuffer.put((byte) (sampleSize >> 8));
	    headerBuffer.put((byte) (sampleSize >> 16));
	    headerBuffer.put((byte) (sampleSize >> 24));

	    headerBuffer.rewind();

	    // lets write header
	    fout.position(0);
	    fout.write(headerBuffer);
	}
    }

    /**
     * Writes supplied data to the Sink (File).
     */
    public abstract void write(ByteBuffer data) throws IOException;

    /**
     * Commit this sink. Causes to prevent any further write operations, and
     * commits temporary file to target. When this returns, Sink is done and
     * cannot be used again.
     */
    public abstract void commit() throws IOException;

}
